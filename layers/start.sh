#!/bin/sh
docker build . -t layer-excersice
docker run --rm layer-excersice
docker history layer-excersice >> history.txt
docker save layer-excersice > layer.tar